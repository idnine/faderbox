/*
	MOVING FADER CONTROL
	ARDUINO SKETCH Version
	by KOO Jonghoe (idnine@gmail.com)
	2013. June
	http://audiocookie.com/EW4SM/movingFader
*/

#include <CapacitiveSensor.h>

// =========================================
//            Arduino PIN Assignment
// =========================================
	// LED
	#define		touchLED	13		// Active LOW
	#define		moveLED		7		// Active LOW
	#define		doneLED		6		// Active LOW
	// MOTOR
	#define		motorEn		12		// SN7544 1-2 EN or 3-4 EN
	#define		motorUp		11		// SN7544 1A or 3A
	#define		motorDn		10		// SN7544 2A or 4A
	// TOUCH SENSOR
	#define		touchRecv	9		// sensor side
	#define		touchSend	8		// resister side
	#define		faderSlide	A1		// Fader Resister Value A1 Analog INPUT
	// External Control Source
	#define		knobPot		A0		// Knob Resister Value A0 Analog INPUT

// =========================================
//                VARIABLES
// =========================================
	// TOUCH SENSOR
	CapacitiveSensor touchSensor = CapacitiveSensor(touchSend, touchRecv);
	volatile bool onTouch = false;

	// KNOB  VARIABLES
	const int knobSample = 10;
	const int knobMargin = 10;
	double knobValue, knobPos;

	// FADER VARIABLES
	const int faderSample = 10;
	const int faderMargin = 10;
	double faderMax, faderMin;
	double faderValue, faderPos;

// =========================================
//                FUNCTIONS
// =========================================
void motorEnable()
{
	digitalWrite(motorEn, HIGH);
}

void motorDisable()
{
	digitalWrite(motorEn, LOW);
}

void faderUp()
{
	digitalWrite(motorUp, HIGH);
	digitalWrite(motorDn, LOW);
}

void faderDn()
{
	digitalWrite(motorUp, LOW);
	digitalWrite(motorDn, HIGH);
}

void motorBreak()
{
	digitalWrite(motorUp, LOW);
	digitalWrite(motorDn, LOW);
}

double faderRead()
{
	double total = 0;
	for(int i=0; i < faderSample; i++)
	{
		total += analogRead(faderSlide);
	}
	return total / faderSample;
}

void faderCaibration()
{
	motorEnable();
	faderUp();
	delay(500);
	faderMax = faderRead();
	faderDn();
	delay(500);
	faderMin = faderRead();
	motorDisable();
}

double knobRead()
{
	double total = 0;
	for(int i=0; i < knobSample; i++)
	{
		total += analogRead(knobPot);
	}
	return (total / knobSample);
}

// =========================================
//               MAIN - SETUP
// =========================================
void setup()
{
	pinMode(touchLED, OUTPUT);
	pinMode(moveLED, OUTPUT);
	pinMode(doneLED, OUTPUT);
	pinMode(motorEn, OUTPUT);
	pinMode(motorUp, OUTPUT);
	pinMode(motorDn, OUTPUT);
	
	digitalWrite(touchLED, HIGH);
	digitalWrite(moveLED, HIGH);
	digitalWrite(doneLED, HIGH);
	
	touchSensor.reset_CS_AutoCal();
	faderCaibration();
	
	faderValue = knobRead();
	knobValue = knobRead();
}

// =========================================
//                MAIN - LOOP
// =========================================
void loop()
{
	onTouch = touchSensor.capacitiveSensorRaw(30) < 350;
	
	if(onTouch)
	{
		digitalWrite(touchLED, LOW);
		digitalWrite(doneLED, HIGH);
		digitalWrite(motorEn, LOW);
		faderPos = faderRead();
		if((faderPos > faderValue + faderMargin) || (faderPos < faderValue - faderMargin))
		{
			faderValue = faderPos;
		}
		if(faderValue > faderMax) faderValue = faderMax;
		if(faderValue < faderMin) faderValue = faderMin;
	}
	else
	{
		digitalWrite(touchLED, HIGH);
		
		// ==================================================
		//                KNOB SECTION
		// --------------------------------------------------
		// CAN CHANGE THIS SECTION to MIDI, OSC, WIFI etc.
		// ==================================================
		knobPos = knobRead();
		if((knobPos > knobValue + knobMargin) || (knobPos < knobValue - knobMargin))
		{
			digitalWrite(touchLED, LOW);
			knobValue = knobPos;
			faderValue = knobValue;
			if(faderValue > faderMax) faderValue = faderMax;
			if(faderValue < faderMin) faderValue = faderMin;
		}
		else
		{
			digitalWrite(touchLED, HIGH);
		}
		
		// ==================================================
		//                  FADER SECTION
		// ==================================================
		faderPos = faderRead();
		if((faderPos >= faderValue - faderMargin) && (faderPos <= faderValue + faderMargin))
		{
			digitalWrite(moveLED, HIGH);
			digitalWrite(doneLED, LOW);
		}
		else
		{
			digitalWrite(moveLED, LOW);
			digitalWrite(doneLED, HIGH);
			digitalWrite(motorEn, HIGH);
			
			while ((faderPos > faderValue + faderMargin) && (faderPos > faderMin + faderMargin) && !onTouch)
			{
				faderDn();
				faderPos = faderRead();
			}
			motorBreak();
			while ((faderPos < faderValue - faderMargin) && (faderPos < faderMax - faderMargin) && !onTouch)
			{
				faderUp();
				faderPos = faderRead();
			}
			motorBreak();
		}
	}
}